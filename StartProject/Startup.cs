﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StartProject.Startup))]
namespace StartProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
